import { IExercise, IWorkout, IWorkoutProgram } from "./exerciseTypes";

const epoch0 = new Date(0).toISOString();
const defaultCreation = {
  created: epoch0,
  modified: epoch0,
};

export const Squat: IExercise = {
  name: "Squat",
  created: epoch0,
  modified: epoch0,
  id: "_squat",
  reps: 5,
  sets: 5,
  weight: 20,
};

export const BarbellRow: IExercise = {
  name: "Barbell row",
  created: epoch0,
  modified: epoch0,
  id: "_barbell_row",
  reps: 5,
  sets: 5,
  weight: 20,
};

export const Deadlift: IExercise = {
  name: "Deadlift",
  created: epoch0,
  modified: epoch0,
  id: "_deadlift",
  reps: 5,
  sets: 1,
  weight: 50,
};

export const OverheadPress: IExercise = {
  name: "Overhead press",
  created: epoch0,
  modified: epoch0,
  id: "_overhead_press",
  reps: 5,
  sets: 5,
  weight: 20,
};

export const BenchPress: IExercise = {
  name: "Bench press",
  created: epoch0,
  modified: epoch0,
  id: "_bench_press",
  reps: 5,
  sets: 5,
  weight: 20,
};

export const WorkoutA: IWorkout = {
  name: "Workout A",
  id: "WorkoutA",
  exercises: [Squat, BenchPress, BarbellRow],
  created: epoch0,
  modified: epoch0,
};

export const WorkoutB: IWorkout = {
  name: "Workout B",
  id: "WorkoutB",
  exercises: [Squat, OverheadPress, Deadlift],
  created: epoch0,
  modified: epoch0,
};

export const DefaultProgram: IWorkoutProgram = {
  ...defaultCreation,
  name: "Default program",
  id: "___default_program",
  workouts: [WorkoutA, WorkoutB],
};
