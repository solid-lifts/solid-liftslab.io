interface IDateInfo {
  created: string;
  modified: string;
}

interface INameId {
  name: string;
  id: string;
}

export interface IExercise extends IDateInfo {
  name: string;
  id: string;
  weight: number | undefined;
  reps: number;
  sets: number;
  link?: string;
  // setRepsCompleted[1] => 3 : 3 reps completed for 2nd set (index 1)
  setRepsCompleted?: Array<number | null>;
}

export interface IWorkout extends IDateInfo {
  name: string;
  id: string;
  exercises: IExercise[];
}

export interface ILoggedWorkouts {
  workouts: IWorkout[];
}

export type TMaybeWorkout = IWorkout | undefined | null;
export type TMaybeExercise = IExercise | undefined | null;
export type TMaybeId = string | undefined | null;

export interface IWorkoutProgram extends INameId, IDateInfo {
  workouts: IWorkout[];
}

export interface IWorkoutStore {
  programs: IWorkoutProgram[];
}
