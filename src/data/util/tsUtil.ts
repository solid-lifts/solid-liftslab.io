export type Option<T> = T | null;
export type Maybe<T> = Option<T> | undefined;
