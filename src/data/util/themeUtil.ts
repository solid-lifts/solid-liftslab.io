export type TTheme = "dark" | "light";

export const setTheme = (theme: TTheme) => {
  console.log("settig theme", theme);
  document.documentElement.className = theme;
};

const getTheme = (t: TTheme) => document.documentElement.classList.contains(t);

export const isDarkMode = () => getTheme("dark");

export const prefersDarkMode = () => {
  const prefersDark = window.matchMedia("(prefers-color-scheme:dark)").matches;
  console.log({ prefersDark });
  return prefersDark;
};

// This observer stuff probably won't work at the moment, so commented out
// export let darkModeVar = prefersDarkMode();
// const setUseDark = (d: boolean) => {
//   darkModeVar = d;
//   console.log(darkModeVar);
//   // d ? setTheme("dark") : setTheme("light");
// };

// // Move dark mode stuff to appdata
// let target = document.documentElement;
// const cb =
//   (onDark: (b: boolean) => void) => (mutationList: MutationRecord[]) => {
//     for (const mutation of mutationList) {
//       if (
//         mutation.type === "attributes" &&
//         mutation.attributeName === "class"
//       ) {
//         const classes = (mutation.target as Element).classList;
//         if (classes.contains("dark")) {
//           onDark(true);
//         } else {
//           onDark(false);
//         }
//       }
//     }
//   };
// const darkModeObserver = new MutationObserver(cb(setUseDark));
// darkModeObserver.observe(target, { attributes: true });
