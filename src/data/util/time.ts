export function dateFormat(isoDate: string | undefined) {
  if (!isoDate) return "";
  const d = new Date(isoDate);
  return `${d.toLocaleDateString()} ${d.toLocaleTimeString()}`;
}

export function nowTimeISO() {
  return new Date().toISOString();
}
