import { Component, createSignal } from "solid-js";
import { DarkModeSelect } from "src/components/DarkModeSelect";
import styles from "./App.module.css";
import { CreateExercise } from "./components/ExerciseCreate";
import { ExerciseList } from "./components/ExerciseList";
import { WorkoutLog } from "./components/WorkoutLog";
import { HomeView } from "./components/HomeView";
import { Button } from "./components/ui/Button";
import { AppDataProvider } from "./context/AppData";
import { setTheme } from "./data/util/themeUtil";
import logo from "./logo.svg";

enum CurrentView {
  "home",
  "createExercise",
  "exerciseList",
  "log",
}

function getTitle(view: CurrentView) {
  switch (view) {
    case CurrentView.home:
      return "Workouts";

    case CurrentView.log:
      return "Log";

    default:
      return "???";
  }
}

const App: Component = () => {
  const [currentView, setCurrenView] = createSignal(CurrentView.home);
  const [currentTitle, setCurrenTitle] = createSignal(getTitle(currentView()));

  const setView = (view: CurrentView) => () => {
    setCurrenView(view);
    setCurrenTitle(getTitle(view));
  };
  const isView = (view: CurrentView) => currentView() === view;

  return (
    <div class={styles.App}>
      <header class={styles.header}>
        <div class="flex items-center gap-1">
          <img src={logo} class={styles.logo} alt="logo" />
          <h1>{currentTitle()}</h1>
        </div>
        <div hidden>
          {/* TODO: Implement proper support */}
          <Button onClick={() => setTheme("dark")}>Dark Theme</Button>
          <Button onClick={() => setTheme("light")}>Light Theme</Button>
          <DarkModeSelect />
        </div>
      </header>

      <main class={styles.main}>
        <div hidden={!isView(CurrentView.createExercise)}>
          <Button onClick={setView(CurrentView.home)}>X</Button>
          <CreateExercise onSave={() => setCurrenView(CurrentView.home)} />
        </div>

        <div hidden={!isView(CurrentView.exerciseList)}>
          <ExerciseList />
        </div>

        <div hidden={!isView(CurrentView.log)}>
          <WorkoutLog />
        </div>

        <div hidden={!isView(CurrentView.home)}>
          <HomeView />
        </div>
      </main>

      <footer>
        <nav class="flex justify-around basis-full">
          <Button onClick={setView(CurrentView.home)}>Home</Button>
          <Button onClick={setView(CurrentView.log)}>Log</Button>
          <Button hidden onClick={setView(CurrentView.exerciseList)}>
            List
          </Button>
          <Button hidden onClick={setView(CurrentView.createExercise)}>
            New
          </Button>
        </nav>
      </footer>
    </div>
  );
};

export default function () {
  return (
    <AppDataProvider>
      <App />
    </AppDataProvider>
  );
}
