import { createMemo, createSignal, For, Show } from "solid-js";
import { Workout } from "src/components/Workout";
import { WorkoutActive } from "src/components/WorkoutActive";
import { WorkoutLogged } from "src/components/WorkoutLogged";
import { useAppData } from "src/context/AppData";
import { TMaybeId } from "src/data/exerciseTypes";
import { dateFormat } from "src/data/util/time";
import styles from "./HomveView.module.css";
import { Button } from "./ui/Button";
import { ModalSlider } from "./ui/ModalSlider";
import { WorkoutCard } from "./WorkoutCard";

export function HomeView() {
  const {
    workoutLog,
    getWorkout,
    activeWorkout: { getActiveWorkoutId, setActiveWorkoutId },
    activeProgram,
  } = useAppData();
  const lastWorkout = () => workoutLog.workouts.at(-1);
  const workouts = () => activeProgram()?.workouts; // can be used to select program later

  const [modalWorkoutId, setModalWorkoutId] = createSignal<string>();
  const [modalLogId, setModalLogId] = createSignal<string>();
  const modalLog = createMemo(() => {
    console.log(modalLogId());
    return getWorkout(modalLogId());
  });

  return (
    <div class={styles.HomeView}>
      <Show when={!getActiveWorkoutId()}>
        {/* Available workouts */}
        <For each={workouts()}>
          {(workout) => (
            <>
              <WorkoutCard
                workout={workout}
                onClick={() => setModalWorkoutId(workout.id)}
              />
            </>
          )}
        </For>

        {/* Last logged workout */}
        <Show when={!!lastWorkout()}>
          <h2 class="text-center">Last workout</h2>
          <WorkoutCard
            date={dateFormat(lastWorkout()?.created)}
            workout={lastWorkout()}
            onClick={() => {
              const id = lastWorkout()?.id;
              id && setModalLogId(id);
            }}
          />
        </Show>

        <StartWorkoutModal
          id={modalWorkoutId()}
          onClose={() => setModalWorkoutId(undefined)}
          onStart={() => {
            setActiveWorkoutId(modalWorkoutId());
            setModalWorkoutId(undefined);
          }}
        />

        {/* Modal with logged workout data; extract to component */}
        <ModalSlider
          open={!!modalLogId()}
          onClose={() => setModalLogId(undefined)}
          title={
            <div>
              <div>{modalLog()?.name}</div>
              <div class="text-sm">{dateFormat(modalLog()?.created)}</div>
            </div>
          }
        >
          <WorkoutLogged id={modalLogId()} />
        </ModalSlider>
      </Show>

      {/* UI when there is an active workout */}
      <Show when={!!getActiveWorkoutId()}>
        <h2>Current workout</h2>
        <WorkoutActive
          workoutId={getActiveWorkoutId()!}
          onClose={() => setActiveWorkoutId(null)}
        />
      </Show>
    </div>
  );
}

function StartWorkoutModal(props: {
  id: TMaybeId;
  onStart: () => void;
  onClose: () => void;
}) {
  const { getWorkout } = useAppData();
  const modalWorkoutId = () => props.id;
  const workout = () => getWorkout(modalWorkoutId());

  return (
    <ModalSlider
      open={!!modalWorkoutId()}
      onClose={props.onClose}
      title={workout()?.name}
    >
      <div class="grid gap-8">
        <Workout workoutId={modalWorkoutId()} />
        <div class="grid">
          hello
          <Button onClick={props.onStart}>Start {workout()?.name}</Button>
        </div>
      </div>
    </ModalSlider>
  );
}
