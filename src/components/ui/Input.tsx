import { JSX } from "solid-js/jsx-runtime";
import { useAppData } from "src/context/AppData";
import { isDarkMode } from "src/data/util/themeUtil";
import styles from "./Input.module.css";

export function Input({
  label,
  ...inputProps
}: {
  label?: string;
} & JSX.InputHTMLAttributes<HTMLInputElement>) {
  const id = label ?? String(Math.random());

  const { theme } = useAppData();

  return (
    <div class={styles.Input}>
      <label for={id} hidden={!label}>
        {label}
      </label>
      <input
        id={id}
        class={`
          w-full
          text-sm
          px-4
          py-3
          ${theme.darkMode() ? "bg-gray-900" : ""}
          ${inputProps.disabled ? "bg-color1" : ""}
          border
        border-gray-700
          rounded-lg
          focus:outline-none
          focus:border-purple-400
      `}
        {...inputProps}
      />
    </div>
  );
}
