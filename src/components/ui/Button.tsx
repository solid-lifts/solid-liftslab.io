import { splitProps } from "solid-js";
import { JSX } from "solid-js/jsx-runtime";

export interface IButton extends JSX.ButtonHTMLAttributes<HTMLButtonElement> {
  classes?: string;
}

export function Button(props: IButton) {
  const [local, rest] = splitProps(props, ["class"]);
  if (props.hidden) {
    return null;
  }

  return (
    <button
      type="button"
      class={`
        inline-block
        px-6
        py-2.5
        bg-blue-600
        font-medium
        text-xs
        leading-tight
        uppercase
        rounded
        shadow-md
        hover:bg-blue-700
        hover:shadow-lg
        focus:bg-blue-700
        focus:shadow-lg
        focus:outline-none
        focus:ring-0
        active:bg-blue-800
        active:shadow-lg
        transition
        duration-150
        ease-in-out
        text-white
        ${local.class ? local.class : ""}
      `}
      {...rest}
    >
      {props.children}
    </button>
  );
}

export function XButton(props: IButton) {
  return (
    <button {...props} class="">
      <svg
        xmlns="http://www.w3.org/2000/svg"
        version="1.0"
        viewBox="0 0 100 100"
        width="24"
      >
        <path
          d="m6.39 6.42 87.19 87.19"
          style="fill:none;fill-rule:evenodd;stroke:red;stroke-width:18.05195999;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
        />
        <path
          d="M6.39 93.61 93.83 6.42"
          style="fill:none;fill-rule:evenodd;stroke:red;stroke-width:17.80202103;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
        />
      </svg>
    </button>
  );
}
