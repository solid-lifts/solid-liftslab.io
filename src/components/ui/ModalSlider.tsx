import { Show } from "solid-js";
import { JSX } from "solid-js/jsx-runtime";
import { XButton } from "./Button";
import styles from "./ModalSlider.module.css";

export function ModalSlider(props: {
  open: boolean;
  title?: JSX.Element;
  onClose: () => void;
  children: JSX.Element;
}) {
  return (
    <Show when={props.open}>
      <div
        class={[
          //
          props.open ? styles.show : styles.hide,
          styles.ModalSliderBackdrop,
        ].join(" ")}
        onClick={props.onClose}
      >
        <div
          class={[
            //
            props.open ? styles.show : styles.hide,
            styles.ModalSlider,
          ].join(" ")}
          onClick={(e) => e.stopPropagation()}
        >
          <div class={styles.TopBar}>
            <div class={styles.Title}>{props.title}</div>
            <XButton onClick={props.onClose} />
          </div>

          {props.open && <div class={styles.Content}>{props.children}</div>}
        </div>
      </div>
    </Show>
  );
}
