import { For } from "solid-js";
import { useAppData } from "src/context/AppData";

export interface ISelectOption {
  id: string;
  label: string;
  selected?: boolean;
  disabled?: boolean;
  onClick: () => void;
}

export function Select(props: {
  disabled?: boolean;
  options: ISelectOption[];
}) {
  const { theme } = useAppData();

  return (
    <select
      // FIXME: Handle onChange instead
      class={`
          text-color1
          text-sm
          p-3
          m-1
          ${theme.darkMode() ? "bg-gray-900" : ""}
          ${props.disabled ? "bg-color1" : ""}
          border
        border-gray-700
          rounded-lg
          focus:outline-none
          focus:border-purple-400
      `}
      style={{ "-webkit-appearance": "none" }}
    >
      <For each={props.options}>
        {(option) => (
          <option
            id={option.id}
            selected={option.selected}
            onClick={option.onClick}
          >
            {option.label}
          </option>
        )}
      </For>
    </select>
  );
}
