import { JSXElement } from "solid-js";

export function DebugInfo<T extends object>(props: {
  data: T | undefined;
  open?: boolean;
  title?: JSXElement;
  hidden?: boolean;
}) {
  return (
    <details hidden={props.hidden} class="cursor-pointer" open={props.open}>
      <summary class={`px-1`}>{props.title}</summary>
      <div class="box-border-1">
        <code class="font-mono whitespace-pre-wrap overflow-auto">
          {JSON.stringify(props.data, null, 2)}
        </code>
      </div>
    </details>
  );
}
