import { JSXElement } from "solid-js";
import { IButton } from "./Button";

export function CircleButton(
  props: {
    outline?: "base" | "border";
    children: JSXElement;
  } & IButton
) {
  if (props.outline === "border") {
    return (
      <button
        type="button"
        onClick={props.onClick}
        class="
          inline-block
          p-3
          text-indigo-600
          border
          border-indigo-600
          rounded-full
          hover:text-white
          hover:bg-indigo-600
          active:bg-indigo-500
          focus:outline-none
          focus:ring
          font-mono
        "
      >
        {props.children}
      </button>
    );
  }

  return (
    <button
      type="button"
      onClick={props.onClick}
      class="
        inline-block
        p-3
        text-white
        bg-indigo-600
        border
        border-indigo-600
        rounded-full
        hover:bg-transparent
        hover:text-indigo-600
        active:text-indigo-500
        focus:outline-none
        focus:ring
        font-mono
      "
    >
      {props.children}
    </button>
  );
}
