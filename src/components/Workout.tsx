import { createSignal, For, JSXElement } from "solid-js";
import { ExerciseEdit } from "src/components/ExerciseEdit";
import { DebugInfo } from "src/components/ui/DebugInfo";
import { ModalSlider } from "src/components/ui/ModalSlider";
import { useAppData } from "src/context/AppData";
import { IExercise, TMaybeExercise, TMaybeId } from "src/data/exerciseTypes";

type TClickField = "sets" | "reps" | "weight" | "row";

export function Workout(props: {
  workoutId: TMaybeId;
  rowExtra?: (e: IExercise) => JSXElement;
}) {
  const { updateExercise, workoutStore } = useAppData();
  const program = () => workoutStore.programs?.[0];
  const workouts = () => program()?.workouts;
  const currentWorkout = () =>
    workouts()?.find((w) => w.id === props.workoutId);

  const [editContents, setEditContents] = createSignal<TMaybeExercise>(null);

  const handleClick =
    (exerciseId: string) => (type: TClickField, index?: number) => {
      console.log(type, exerciseId, index);
      switch (type) {
        case "reps":
        case "sets":
        case "weight":
        case "row":
          const e = currentWorkout()?.exercises.find(
            (ex) => ex.id === exerciseId
          );
          setEditContents(e);
          break;

        default:
          setEditContents(undefined);
          break;
      }
    };

  function onExerciseSave(newE: IExercise) {
    const cwo = currentWorkout();
    if (cwo) {
      const exercises = cwo.exercises.map((ex) =>
        newE.id === ex.id ? newE : ex
      );
      // setCurrentWorkout({ ...cwo, exercises });
      updateExercise(cwo.id, newE.id, newE);
    } else {
      throw Error("No valid workout");
    }
  }

  if (!props.workoutId) {
    console.log("NO ID");
    return null;
  }

  return (
    <div class="grid gap-10">
      {/* <For each={currentWorkout()?.exercises}> */}
      <For each={currentWorkout()?.exercises}>
        {(e) => (
          <WorkoutRow
            exercise={e}
            onClick={handleClick(e.id)}
            rowExtra={props.rowExtra}
          />
        )}
      </For>

      <DebugInfo data={currentWorkout()} title={"Local data"} />

      <ModalSlider
        open={!!editContents()}
        onClose={() => setEditContents(undefined)}
        // title={`Active ${props.currentWorkout?.name}`}
        title={`Active ${currentWorkout()?.name}`}
      >
        {editContents() && (
          <ExerciseEdit
            data={editContents()!}
            onCancel={() => setEditContents(undefined)}
            onSave={(e) => {
              setEditContents(undefined);
              onExerciseSave(e);
            }}
          />
        )}
      </ModalSlider>
    </div>
  );
}

function WorkoutRow(props: {
  exercise: IExercise;
  onClick: (type: TClickField, index?: number) => void;
  rowExtra?: (e: IExercise) => JSXElement;
}) {
  return (
    <div class="flex flex-wrap gap-2">
      <div
        class={`
          basis-full
          flex
          justify-between
          border
          border-slate-500
          rounded
          p-2
          cursor-pointer
        `}
        onClick={() => props.onClick("row")}
      >
        <div>{props.exercise.name}</div>
        <div class="flex gap-2">
          <button onClick={() => props.onClick("sets")}>
            {props.exercise.sets}
          </button>
          x
          <button onClick={() => props.onClick("reps")}>
            {props.exercise.reps}
          </button>
          <button onClick={() => props.onClick("weight")}>
            {props.exercise.weight}kg
          </button>
        </div>
      </div>

      {props.rowExtra?.(props.exercise)}
    </div>
  );
}
