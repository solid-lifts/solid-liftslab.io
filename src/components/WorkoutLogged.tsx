import { createSignal, For, JSXElement, Show } from "solid-js";
import { ExerciseEdit } from "src/components/ExerciseEdit";
import { DebugInfo } from "src/components/ui/DebugInfo";
import { ModalSlider } from "src/components/ui/ModalSlider";
import { useAppData } from "src/context/AppData";
import { IExercise, TMaybeExercise, TMaybeId } from "src/data/exerciseTypes";

type TClickField = "sets" | "reps" | "weight" | "row";

export function LoggedWorkout(props: { id: TMaybeId }) {
  const { workoutLog } = useAppData();
  const loggedWorkout = () =>
    workoutLog.workouts.find((w) => w.id === props.id);
  return (
    <div>
      <DebugInfo title={"Data"} data={loggedWorkout()} />
    </div>
  );
}

export function WorkoutLogged(props: {
  id: TMaybeId;
  rowExtra?: (e: IExercise) => JSXElement;
}) {
  const { updateExercise, workoutStore } = useAppData();
  const { workoutLog } = useAppData();
  const loggedWorkout = () =>
    workoutLog.workouts.find((w) => w.id === props.id);

  const [editContents, setEditContents] = createSignal<TMaybeExercise>(null);

  const handleClick =
    (exerciseId: string) => (type: TClickField, index?: number) => {
      console.log(type, exerciseId, index);
      switch (type) {
        case "reps":
        case "sets":
        case "weight":
        case "row":
          const e = loggedWorkout()?.exercises.find(
            (ex) => ex.id === exerciseId
          );
          // TODO: Save/Edit logged workout
          // setEditContents(e);
          break;

        default:
          setEditContents(undefined);
          break;
      }
    };

  function onExerciseSave(newE: IExercise) {
    const cwo = loggedWorkout();
    if (cwo) {
      const exercises = cwo.exercises.map((ex) =>
        newE.id === ex.id ? newE : ex
      );
      // updateExercise(cwo.id, newE.id, newE);
      // TODO: Save/Edit logged workout
      console.warn("Not implemented!");
    } else {
      throw Error("No valid workout");
    }
  }

  if (!props.id) {
    console.log("NO ID");
    return null;
  }

  return (
    <div class="grid gap-10">
      <For each={loggedWorkout()?.exercises}>
        {(e) => (
          <WorkoutRow
            exercise={e}
            onClick={handleClick(e.id)}
            // rowExtra={props.rowExtra}
            // TODO: Show graphical representation
            rowExtra={(e) => (
              <Show when={e.setRepsCompleted}>
                <div>
                  Sets completed: {
                    e.setRepsCompleted?.filter((s) => Number(s) >= e.reps)
                      .length
                  }
                  /{e.sets}
                </div>
              </Show>
            )}
          />
        )}
      </For>
      <DebugInfo title={"Data"} data={loggedWorkout()} />

      <ModalSlider
        open={!!editContents()}
        onClose={() => setEditContents(undefined)}
        // title={`Active ${props.currentWorkout?.name}`}
        title={`Active ${loggedWorkout()?.name}`}
      >
        {editContents() && (
          <ExerciseEdit
            data={editContents()!}
            onCancel={() => setEditContents(undefined)}
            onSave={(e) => {
              setEditContents(undefined);
              onExerciseSave(e);
            }}
            nonEditable={{ name: true, reps: true, sets: true, weight: true }}
          />
        )}
      </ModalSlider>
    </div>
  );
}

function WorkoutRow(props: {
  exercise: IExercise;
  onClick: (type: TClickField, index?: number) => void;
  rowExtra?: (e: IExercise) => JSXElement;
}) {
  return (
    <div class="flex flex-wrap gap-2">
      <div
        class={`
          basis-full
          flex
          justify-between
          border
          border-slate-500
          rounded
          p-2
          cursor-pointer
        `}
        onClick={() => props.onClick("row")}
      >
        <div>{props.exercise.name}</div>
        <div class="flex gap-2">
          <button onClick={() => props.onClick("sets")}>
            {props.exercise.sets}
          </button>
          x
          <button onClick={() => props.onClick("reps")}>
            {props.exercise.reps}
          </button>
          <button onClick={() => props.onClick("weight")}>
            {props.exercise.weight}kg
          </button>
        </div>
      </div>

      {props.rowExtra?.(props.exercise)}
    </div>
  );
}
