import { ISelectOption, Select } from "src/components/ui/Select";
import { useAppData } from "src/context/AppData";
import { TTheme } from "src/data/util/themeUtil";

export function DarkModeSelect() {
  const { theme } = useAppData();
  const options: (ISelectOption & { id: TTheme | "os" })[] = [
    {
      id: "dark",
      label: "Dark Mode",
      selected: theme.darkMode(),
      onClick: () => theme.setDarkMode(true),
    },
    {
      id: "light",
      label: "Light Mode",
      selected: theme.darkMode() === false,
      onClick: () => theme.setDarkMode(false),
    },
    // {
    //   id: "os",
    //   label: "OS",
    //   selected: theme.darkMode() === undefined,
    //   onClick: () => theme.setDarkMode(undefined),
    // },
  ];
  return <Select options={options} />;
}
