import { For } from "solid-js";
import { IExercise, IWorkout } from "src/data/exerciseTypes";
import styles from "./WorkoutCard.module.css";

export function WorkoutCard(props: {
  workout: IWorkout | undefined | null;
  onClick?: () => void;
  noFrame?: boolean;
  date?: string;
}) {
  return (
    <section
      class={[
        //
        styles.WorkoutCard,
        props.noFrame ? styles.NoFrame : "",
      ].join(" ")}
      onClick={props.onClick}
    >
      <div class="flex flex-wrap justify-between place-items-center">
        <div class={styles.title}>{props.workout?.name ?? "-"}</div>
        <div hidden={!props.date} class="text-xs">
          {props.date}
        </div>
      </div>
      <div class={styles.ExerciseGrid}>
        <For each={props.workout?.exercises} fallback={<div>No items</div>}>
          {(item, index) => <ExersizeRow data-index={index()} e={item} />}
        </For>
      </div>
    </section>
  );
}

function ExersizeRow(props: { e: IExercise }) {
  return (
    <div class={styles.ExerciseRow}>
      <div>{props.e.name}</div>
      <div>
        {props.e.sets}x{props.e.reps}
      </div>
      <div>{props.e.weight}kg</div>
    </div>
  );
}
