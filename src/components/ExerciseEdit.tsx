import { createEffect, createSignal } from "solid-js";
import { Button } from "src/components/ui/Button";
import { Input } from "src/components/ui/Input";
import { IExercise } from "src/data/exerciseTypes";
import { nowTimeISO } from "src/data/util/time";

export function ExerciseEdit(props: {
  data: IExercise;
  onCancel: () => void;
  onSave: (e: IExercise) => void;
  nonEditable?: {
    name?: boolean;
    weight?: boolean;
    sets?: boolean;
    reps?: boolean;
  };
  saveDisabled?: boolean;
}) {
  const [exercise, setExercise] = createSignal(props.data);
  // Ensure modified date is current:
  createEffect(() => setExercise((e) => ({ ...e, modified: nowTimeISO() })));

  function handleSave() {
    console.log("saving", exercise());
    props.onSave(exercise());
  }
  return (
    <form
      class="grid gap-4"
      onSubmit={(e) => {
        e.preventDefault();
        handleSave();
      }}
    >
      <div>Modified {exercise().modified}</div>
      <Input
        label="Name"
        required
        value={exercise().name}
        onInput={(e) => {
          setExercise((current) => ({
            ...current,
            name: e.currentTarget.value,
          }));
        }}
        disabled={props.nonEditable?.name}
      />

      <Input
        label="Weight"
        placeholder="20 kgs"
        required
        type="number"
        min="0"
        step={2.5}
        value={exercise().weight}
        onInput={(e) => {
          setExercise((current) => ({
            ...current,
            weight: e.currentTarget.valueAsNumber,
          }));
        }}
        disabled={props.nonEditable?.weight}
      />

      <Input
        label="Sets"
        required
        type="number"
        min="1"
        value={exercise().sets}
        onInput={(e) => {
          setExercise((current) => ({
            ...current,
            sets: e.currentTarget.valueAsNumber,
          }));
        }}
        disabled={props.nonEditable?.sets}
      />

      <Input
        label="Reps"
        required
        type="number"
        min="1"
        value={exercise().reps}
        onInput={(e) => {
          setExercise((current) => ({
            ...current,
            reps: e.currentTarget.valueAsNumber,
          }));
        }}
        disabled={props.nonEditable?.reps}
      />

      <div class="flex justify-around">
        <Button onClick={props.onCancel}>Cancel</Button>
        <Button type="submit" disabled={props.saveDisabled}>
          Save
        </Button>
      </div>
    </form>
  );
}
