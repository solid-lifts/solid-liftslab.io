import { createSignal, For } from "solid-js";
import { DebugInfo } from "src/components/ui/DebugInfo";
import { WorkoutLogged } from "src/components/WorkoutLogged";
import { useAppData } from "src/context/AppData";
import { IWorkout } from "src/data/exerciseTypes";
import { dateFormat } from "src/data/util/time";
import { Button } from "./ui/Button";
import { ModalSlider } from "./ui/ModalSlider";
import { WorkoutCard } from "./WorkoutCard";

export function WorkoutLog() {
  const { workoutLog, removeLoggedWorkout } = useAppData();
  const workouts = () => workoutLog.workouts;

  const [modalLog, setModalLog] = createSignal<IWorkout>();

  function handleWorkoutClick(id: IWorkout["id"]) {
    console.log("clicked", id);
    setModalLog(workouts()?.find((w) => w.id === id));
  }

  return (
    <div class="grid gap-4">
      <For each={workouts()}>
        {(workout) => (
          <div class="grid gap-2 my-2">
            <WorkoutCard
              workout={workout}
              onClick={() => handleWorkoutClick(workout.id)}
            />
            <DebugInfo title="Data" data={workout} />
          </div>
        )}
      </For>

      <ModalSlider
        open={!!modalLog()}
        onClose={() => setModalLog(undefined)}
        title={
          <div>
            <div>{modalLog()?.name}</div>
            <div class="text-sm">{dateFormat(modalLog()?.created)}</div>
          </div>
        }
      >
        <div class="grid gap-2">
          <Button
            onClick={() => {
              removeLoggedWorkout(modalLog()?.id ?? "");
              setModalLog(undefined);
            }}
          >
            Remove
          </Button>

          <WorkoutLogged id={modalLog()?.id} />
          {/* TODO: Use more graphical view */}
          {/* <DebugInfo open title="Data" data={modalLog()} /> */}
        </div>
      </ModalSlider>
    </div>
  );
}
