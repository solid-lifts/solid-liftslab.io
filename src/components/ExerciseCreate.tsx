import { createSignal } from "solid-js";
import { IExercise } from "../data/exerciseTypes";
import styles from "./ExerciseCreate.module.css";
import { Input } from "./ui/Input";

const initial: IExercise = {
  name: "",
  created: "",
  modified: "",
  id: "",
  weight: 20,
  reps: 5,
  sets: 5,
};

export function CreateExercise({ onSave }: { onSave: () => void }) {
  const [exercise, setExercise] = createSignal(initial);

  function handleSave() {
    console.log(exercise());
    onSave();
  }

  return (
    <form
      class={styles.ECreate}
      onSubmit={(e) => {
        e.preventDefault();
        onSave();
      }}
    >
      <Input
        label="Name"
        required
        value={exercise().name}
        onInput={(e) => {
          setExercise((current) => ({
            ...current,
            name: e.currentTarget.value,
          }));
        }}
      />

      <Input
        label="Weight"
        placeholder="20 kgs"
        required
        type="number"
        value={exercise().weight}
        onInput={(e) => {
          setExercise((current) => ({
            ...current,
            weight: e.currentTarget.valueAsNumber,
          }));
        }}
      />

      <Input
        label="Sets"
        required
        type="number"
        value={exercise().sets}
        onInput={(e) => {
          setExercise((current) => ({
            ...current,
            sets: e.currentTarget.valueAsNumber,
          }));
        }}
      />

      <Input
        label="Reps"
        required
        type="number"
        value={exercise().reps}
        onInput={(e) => {
          setExercise((current) => ({
            ...current,
            reps: e.currentTarget.valueAsNumber,
          }));
        }}
      />

      <button type="submit">Save</button>
    </form>
  );
}
