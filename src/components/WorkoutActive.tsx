import { createEffect, createSignal, For } from "solid-js";
import { Button } from "src/components/ui/Button";
import { CircleButton } from "src/components/ui/CircleButton";
import { Workout } from "src/components/Workout";
import { useAppData } from "src/context/AppData";
import { IExercise, IWorkout } from "src/data/exerciseTypes";

export function WorkoutActive(props: {
  workoutId: string;
  onClose: () => void;
}) {
  const { addLoggedWorkout, activeProgram } = useAppData();
  const workout = () =>
    activeProgram()?.workouts.find((w) => w.id === props.workoutId);
  const exercises = () => workout()?.exercises ?? [];

  function handleSave() {
    const w = workout();
    const l = setReps();
    if (!w || !l) {
      console.error("undefined value", w, l);
      // XXX: Show error to user? This should never happen, though...
      return;
    }

    const updatedExercises = w.exercises.map((e) => {
      const found = l.find((setRep) => setRep.id === e.id);
      if (!found) return e;
      return { ...e, setRepsCompleted: found.completedReps };
    });
    console.log(updatedExercises);
    if (w) {
      addLoggedWorkout({...w, exercises: updatedExercises});
    }
    props.onClose();
  }

  const [setReps, setSetReps] = createSignal(initSetRep(exercises()));
  createEffect(() => {
    if (!!exercises()) {
      setSetReps(initSetRep(exercises()));
    }
  });

  function setRepClick(id: string, index: number, repsLeft: number | null) {
    console.log({ id, index, repsLeft });
    setSetReps((a): IMSetRep[] | undefined => {
      return a?.map((item) => {
        if (item.id !== id) return item;
        const ret = {
          id: item.id,
          maxReps: item.maxReps,
          completedReps: item.completedReps?.map((completedReps, i) => {
            if (i !== index) return completedReps;
            if (completedReps === 0) return item.maxReps;
            if (completedReps === item.maxReps) return 0;
            return 0; // reset
          }),
        };
        return ret;
      });
    });
  }

  return (
    <div class="grid gap-4">
      <Workout
        workoutId={props.workoutId}
        rowExtra={(e) => (
          <For each={setReps()?.find((i) => i.id === e.id)?.completedReps}>
            {(completedReps, index) => (
              <ExerciseReps
                onClick={() => {
                  setRepClick(e.id, index(), completedReps);
                }}
                fill={e.reps - Number(completedReps) === 0}
              >
                {e.reps - Number(completedReps)}
              </ExerciseReps>
            )}
          </For>
        )}
      />

      <Button onClick={props.onClose}>Abort</Button>

      <Button onClick={handleSave}>Save</Button>
    </div>
  );
}

function ExerciseReps(props: {
  fill?: boolean;
  children: number;
  onClick: () => void;
}) {
  return (
    <CircleButton
      outline={props.fill ? "base" : "border"}
      onClick={() => props.onClick()}
    >
      {props.children === 0 ? "✓" : props.children}
    </CircleButton>
  );
}

function ExerciseReps2(props: {
  repsComplete: number;
  reps: number;
  onClick: () => void;
}) {
  return (
    <CircleButton outline="border" onClick={() => props.onClick()}>
      {/* {props.reps - props.repsComplete} */}
      {props.repsComplete}
    </CircleButton>
  );
}

function updateWorkoutWithExercise(wo: IWorkout, ex: IExercise): IWorkout {
  const updatedExercises = wo.exercises.map((e) => (e.id === ex.id ? ex : e));
  return { ...wo, exercises: updatedExercises };
}

function createRepsArray(sets: number, reps: number): number[] {
  return Array.from(Array(sets)).map((_) => 0);
}

interface IMSetRep {
  id: string;
  completedReps: IExercise["setRepsCompleted"];
  maxReps: number;
}
function initSetRep(exercises: IExercise[]): IMSetRep[] | undefined {
  return exercises?.map((e) => ({
    id: e.id,
    completedReps: createRepsArray(e.sets, e.reps),
    maxReps: e.reps,
  }));
}
