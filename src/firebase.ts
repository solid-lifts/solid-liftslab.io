// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBIbwqjbNL_aP-wZ5Y45RLRyCds2rZox4w",
  authDomain: "solid-lifts.firebaseapp.com",
  projectId: "solid-lifts",
  storageBucket: "solid-lifts.appspot.com",
  messagingSenderId: "110142129473",
  appId: "1:110142129473:web:246c95f40eacba8bf3cdc0",
  measurementId: "G-FXYZN2MQS3"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
