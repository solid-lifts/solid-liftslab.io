import { createContext, JSXElement, useContext } from "solid-js";
import {
  IExercise,
  ILoggedWorkouts,
  IWorkout,
  IWorkoutProgram,
  IWorkoutStore,
  TMaybeExercise,
  TMaybeId,
  TMaybeWorkout,
} from "src/data/exerciseTypes";
import {
  createActiveWorkoutStore,
  createDarkModeStore,
  createWorkoutLogStore,
  createWorkoutStore,
} from "src/store/solidStore";

const AppDataContext = createContext({
  // Log
  get workoutLog(): ILoggedWorkouts {
    return { workouts: [] };
  },
  addLoggedWorkout: (w: IWorkout) => {},
  removeLoggedWorkout: (id: string) => {},

  // Workouts
  get workoutStore(): IWorkoutStore {
    return { programs: [] };
  },
  activeProgram: (id?: number): null | IWorkoutProgram => null,
  workouts(): IWorkout[] | null {
    return null;
  },
  getWorkout: (wId: TMaybeId): TMaybeWorkout => undefined,
  getExercise: (exId: string): TMaybeExercise => undefined,
  updateExercise(wId: string, exId: string, data: IExercise): TMaybeExercise {
    return null;
  },
  theme: {
    setDarkMode: (b: boolean | undefined) => {},
    darkMode(): boolean | undefined {
      return undefined;
    },
  },
  // Active workout
  activeWorkout: {
    getActiveWorkoutId: (): TMaybeId => undefined,
    setActiveWorkoutId: (id: TMaybeId) => {},
    // getActiveWorkout: (): TMaybeWorkout => null,
  },
});

export function AppDataProvider(props: { children: JSXElement }) {
  const [workoutLog, setWorkoutLog] = createWorkoutLogStore();
  const [workoutStore, setWorkoutStore] = createWorkoutStore();
  const activeWorkout = createActiveWorkoutStore();
  const { darkMode, setDarkMode } = createDarkModeStore();
  const activeProgram = (id: number = 0) => workoutStore.programs[id];

  function addLoggedWorkout(workout: IWorkout) {
    const currentWorkouts = workoutLog.workouts;
    const date = new Date().toISOString();
    const workoutToAdd: IWorkout = {
      ...workout,
      id: date,
      created: date,
      modified: date,
    };
    setWorkoutLog({ workouts: [...currentWorkouts, workoutToAdd] });
  }

  function removeLoggedWorkout(wId: string) {
    setWorkoutLog(({ workouts }) => {
      return { workouts: workouts.filter((w) => w.id !== wId) };
    });
  }

  function updateExercise(
    workoutId: string,
    exerciseId: string,
    data: IExercise
  ) {
    console.log(">>> Updating exercise");
    const workout = getWorkout(workoutId);
    if (!workout) {
      console.error("Found no matching workout!");
      return;
    }
    const exercise = getExercise(exerciseId);
    if (!exercise) {
      console.error("Found no matching exercise!");
      return;
    }
    const workoutExercises = workout?.exercises.map((e) => {
      if (e.id !== exerciseId) return e;
      return data;
    });
    const updatedProgram: IWorkoutProgram = {
      ...activeProgram(),
      workouts: activeProgram().workouts.map((w) => {
        if (w.id !== workoutId) return w;
        console.group(">>> Updating", w.name);
        return { ...w, exercises: workoutExercises };
      }),
    };

    console.log(">>> was", { ...data });
    console.log(">>> is", { ...exercise });
    console.log(">>> diff", diff(data, exercise));
    console.log(">>> diff", compareJSON(data, exercise));
    setWorkoutStore(() => ({ programs: [updatedProgram] }));
    return data;
  }

  function getExercise(id: string): TMaybeExercise {
    const allExercises = activeProgram().workouts.flatMap((w) => w.exercises);
    const e = allExercises.find((w) => w.id === id);
    console.log("getting", e?.id, e?.name);
    return e;
  }

  function getWorkout(wid: TMaybeId): TMaybeWorkout {
    return activeProgram().workouts.find((w) => w.id === wid);
  }

  return (
    <AppDataContext.Provider
      value={{
        workoutLog,
        addLoggedWorkout,
        removeLoggedWorkout,
        workoutStore,
        workouts: () => activeProgram().workouts,
        activeProgram,
        getWorkout,
        getExercise,
        updateExercise,
        theme: {
          darkMode,
          setDarkMode,
        },
        activeWorkout,
      }}
    >
      {props.children}
    </AppDataContext.Provider>
  );
}

export function useAppData() {
  return useContext(AppDataContext);
}

//http://stackoverflow.com/questions/679915/how-do-i-test-for-an-empty-javascript-object
var isEmptyObject = function (obj: any) {
  var name;
  for (name in obj) {
    return false;
  }
  return true;
};

//http://stackoverflow.com/questions/8431651/getting-a-diff-of-two-json-objects
var diff = function (obj1: any, obj2: any) {
  var result = {};
  var change;
  for (var key in obj1) {
    if (typeof obj2[key] == "object" && typeof obj1[key] == "object") {
      change = diff(obj1[key], obj2[key]);
      if (isEmptyObject(change) === false) {
        // @ts-ignore
        result[key] = change;
      }
    } else if (obj2[key] != obj1[key]) {
      // @ts-ignore
      result[key] = obj2[key];
    }
  }
  return result;
};

var compareJSON = function (obj1: any, obj2: any) {
  var ret = {};
  for (var i in obj2) {
    if (!obj1.hasOwnProperty(i) || obj2[i] !== obj1[i]) {
      // @ts-ignore
      ret[i] = obj2[i];
    }
  }
  return ret;
};
