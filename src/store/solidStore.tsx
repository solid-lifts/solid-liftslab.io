import { createEffect, createSignal } from "solid-js";
import { createStore, SetStoreFunction, Store } from "solid-js/store";
import { DefaultProgram } from "src/data/defaultExercises";
import {
  IWorkout,
  IWorkoutProgram,
  TMaybeId,
  TMaybeWorkout,
} from "src/data/exerciseTypes";
import { prefersDarkMode, setTheme } from "src/data/util/themeUtil";

export function createLocalStore<T extends object>(
  name: string,
  init: T,
  dataStore: Storage = localStorage
): [Store<T>, SetStoreFunction<T>] {
  const localState = dataStore.getItem(name);
  const [state, setState] = createStore<T>(
    localState ? JSON.parse(localState) : init
  );
  createEffect(() => dataStore.setItem(name, JSON.stringify(state)));
  return [state, setState];
}

export function removeIndex<T>(array: readonly T[], index: number): T[] {
  return [...array.slice(0, index), ...array.slice(index + 1)];
}

export function createActiveWorkoutStore<T extends object>() {
  const init: { workoutId: TMaybeId } = { workoutId: null };
  const [state, setState] = createLocalStore(
    "com.imolit.workoutActive",
    init,
    sessionStorage
  );
  return {
    setActiveWorkoutId: (id: TMaybeId) => setState({ workoutId: id }),
    getActiveWorkoutId: () => state.workoutId,
  };
}

export function createWorkoutLogStore() {
  const init: { workouts: IWorkout[] } = { workouts: [] };
  return createLocalStore("com.imolit.workoutLog", init, localStorage);
}

export function createWorkoutStore() {
  const init: { programs: IWorkoutProgram[] } = {
    programs: [DefaultProgram],
  };
  return createLocalStore("com.imolit.workoutstore", init, localStorage);
}

export function createDarkModeStore() {
  const key = "darkMode.imolit";
  const stored = sessionStorage.getItem(key);
  const initial =
    stored === null ? prefersDarkMode() : (JSON.parse(stored) as boolean);

  const [darkMode, setDarkMode] = createSignal(initial || true);
  createEffect(() => {
    darkMode() == null
      ? sessionStorage.removeItem(key)
      : sessionStorage.setItem(key, JSON.stringify(darkMode()));
    darkMode() == null || !!darkMode() ? setTheme("dark") : setTheme("light");
  });
  return {
    darkMode: () => {
      return darkMode() ?? prefersDarkMode();
    },
    setDarkMode,
  };
}
